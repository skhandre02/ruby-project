class StringPrograms

  def initialize(string)
    @input = string
  end

  def is_palidrome
    return @input.eql? @input.reverse
  end

  def set_input(input) # setter method for input instance variable
    @input = input
  end

  def occurrences_of_each_character
    occurrences = {}
    @input.each_char do |ch|
      if (occurrences.has_key?(ch))
        count = occurrences.fetch(ch) # another way to get value is occurrences[ch]
        occurrences.store(ch,count+1)
      else
        occurrences.store(ch,1)
      end
    end
    occurrences = occurrences.sort_by {|k, v| -v} # where v represents ascending order and -v represents descending order
    return occurrences
  end

  def is_anagram(stringToCheck)
    return @input.chars.sort.eql? stringToCheck.chars.sort
  end

  input = "madam"
  object = StringPrograms.new(input)
  puts ("#{input} is Palindrome: "+ object.is_palidrome.to_s)
  puts ("Character occurrences for string \"#{input}\": " + object.occurrences_of_each_character.to_s)
  input = "GLOBANT INDIA PVT. LTD."
  object.set_input(input)
  puts ("#{input} is Palindrome: "+ object.is_palidrome.to_s)
  puts ("Character occurrences for string \"#{input}\": " + object.occurrences_of_each_character.to_s)

  input= "listen"
  object.set_input input
  puts (" \"#{input}\" & \"silent\" are anagram: " + object.is_anagram("silent").to_s)


  puts (" \"#{input}\" & \"globant\" are anagram: " + object.is_anagram("globant").to_s)

end
