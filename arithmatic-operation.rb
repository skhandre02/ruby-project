class ArithmaticOperation

	def initialize(number1, number2)
		@number1 =  number1
		@number2 = number2
	end

	def display_operands
		puts "First number is #{@number1} and another number is #{@number2}"
	end

	def add
		printf "Addition is: "
		puts @number1 + @number2
	end

	def subtract
		printf "Subtraction is: "
		puts @number1 - @number2
	end

	def multiply
		printf "Multiplication is: "
		puts @number1 * @number2
	end

	def divide
		printf "Division is: "
		puts @number1 / @number2
	end

	def modulo
		printf "Modulo is: "
		puts @number1 % @number2
	end

  operation = ArithmaticOperation.new(20, 10)
	operation.display_operands
	operation.add
	operation.subtract
	operation.multiply
	operation.divide
	operation.modulo

end

#
# num1 = 10
# num2 = 15
# puts num2 % num1
# puts num2.odd?
# puts num1.even?
# sentence = "My name is Swapnil"
# p sentence

# puts "Enter first number : "
# number1 = gets.chomp
# puts "Enter secord Number : ";
# number2 = gets.chomp
#
# puts "Enter operation(+,-,/,%,*)"
# operand = gets.chomp
#
# case operand
# 	when "+"	then puts number1.to_f + number2.to_f
# 	when "-"	then puts number1.to_f + number2.to_f
# 	when "*"	then puts number1.to_f + number2.to_f
# 	when "/"	then puts number1.to_f + number2.to_f
# 	when "%"	then puts number1.to_f + number2.to_f
# end
